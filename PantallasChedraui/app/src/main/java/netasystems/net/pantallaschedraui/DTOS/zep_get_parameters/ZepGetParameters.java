package netasystems.net.pantallaschedraui.DTOS.zep_get_parameters;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.List;

/**
 * @author Neta Systems / Meraz Rodriguez Gonzalo
 */

public class ZepGetParameters implements KvmSerializable {

    /**
     * Declaración de lista y variables
     */

    private TableOfZtmprParam TyTParametros = new TableOfZtmprParam();
    private String Zobj1;
    private String Zprog;


    public ZepGetParameters() {
        Zobj1 = "";
        Zprog = "";
    }

    /**
     * Getters y Setters
     * @return
     */
    public String getZobj1() {
        return Zobj1;
    }

    public void setZobj1(String zobj1) {
        Zobj1 = zobj1;
    }

    public String getZprog() {
        return Zprog;
    }

    public void setZprog(String zprog) {
        Zprog = zprog;
    }





    @Override
    public Object getProperty(int index) {
        switch (index){
            case 0:
                return TyTParametros;
            case 1:
                return this.Zobj1;
            case 2:
                return this.Zprog;
            default:
                break;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void setProperty(int index, Object value) {
        switch (index){
            case 0:
                this.TyTParametros = (TableOfZtmprParam) value;
                break;
            case 1:
                this.Zobj1 = value.toString();
                break;
            case 2:
                this.Zprog = value.toString();
                break;
            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch (index){
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "TyTParametros";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zobj1";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Zprog";
            default:
                break;
        }

    }
}