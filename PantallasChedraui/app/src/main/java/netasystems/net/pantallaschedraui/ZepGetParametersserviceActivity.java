package netasystems.net.pantallaschedraui;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.TableOfZtmprParam;
import netasystems.net.pantallaschedraui.DTOS.zep_get_parameters.ZtmprParam;

public class ZepGetParametersserviceActivity extends AppCompatActivity {

    String nameSpaceSoap = "http://schemas.xmlsoap.org/soap/envelope/";
    String nameSpaceSap = "urn:sap-com:document:sap:soap:functions:mc-style";

    String methodSap = "ZepGetParameters";

    String soapActionSap = "urn:sap-com:document:sap:soap:functions:mc-style/ZepGetParameters";

    String urlSap = "http://chderp00.gcch.com:8010/sap/bc/srt/rfc/sap/zep_get_parameters/300/zep_get_parametersservice/zep_get_parametersservice";

    private TextView txtResultado;
    private Button btnConsulta;

    boolean error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zep_get_parametersservice);

        txtResultado = (TextView) findViewById(R.id.txt_Resultado);
        btnConsulta = (Button) findViewById(R.id.btn_Consulta);

        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TareaWSConsulta task = new TareaWSConsulta();
                task.execute();
            }
        });
    }

    private class TareaWSConsulta extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            }

        @Override
        protected String doInBackground(String... strings) {
            consulta();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(String... values) {

        }
    }

    public void consulta() {

        SoapObject soapObject = new SoapObject(nameSpaceSap,methodSap);

        PropertyInfo propertyInfoZobj1 = new PropertyInfo();
        PropertyInfo propertyInfoZprog = new PropertyInfo();

        propertyInfoZobj1.setName("Zobj1");
        propertyInfoZobj1.setValue("VERSION");
        propertyInfoZobj1.setType(String.class);
        soapObject.addProperty(propertyInfoZobj1);

        propertyInfoZprog.setName("Zprog");
        propertyInfoZprog.setValue("AUDITORIAPRECIOS");
        propertyInfoZprog.setType(String.class);
        soapObject.addProperty(propertyInfoZprog);

        TableOfZtmprParam tableOfZtmprParam = new TableOfZtmprParam();
        soapObject.addProperty("TyTParametros", tableOfZtmprParam);

        ZtmprParam tableOfZtmprParam2 = new ZtmprParam();
        soapObject.addProperty("item", tableOfZtmprParam2);

        SoapSerializationEnvelope envelopeSap = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelopeSap.env = nameSpaceSoap;
        envelopeSap.dotNet = false;
        envelopeSap.implicitTypes = true;

        envelopeSap.addMapping(nameSpaceSap,"TyTParametros", new TableOfZtmprParam().getClass());
        envelopeSap.addMapping(nameSpaceSap, "item", new ZtmprParam().getClass());

        envelopeSap.setOutputSoapObject(soapObject);

        HttpTransportSE httpTransportSE = new HttpTransportSE(urlSap);
        httpTransportSE.debug = true;

        List<HeaderProperty> c = new ArrayList<HeaderProperty>();
        //c.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("vcarmona:pale1481".getBytes())));

       // c.add(new HeaderProperty("Username",org.kobjects.base64.Base64.encode("vcarmona".getBytes())));
        //c.add(new HeaderProperty("Password",org.kobjects.base64.Base64.encode("pale1481".getBytes())));

        //String test = org.kobjects.base64.Base64.encode("vcarmona:pale1481".getBytes());
        //System.out.println("---Base64----< "+test+" >");
        c.add(new HeaderProperty("Authorization","Basic dmNhcm1vbmE6cGFsZTE0ODE="));

        try {
            httpTransportSE.call(soapActionSap,envelopeSap, c);

            Log.d("dump Request: ",httpTransportSE.requestDump);
            Log.d("dump Respondes: ",httpTransportSE.responseDump);

            //SoapObject response = (SoapObject) envelopeSap.getResponse();
            SoapObject response = (SoapObject) envelopeSap.bodyIn;

        } catch (IOException ioe) {
            Log.d("IOException", ioe.toString());
            //Toast.makeText(getApplicationContext(),"No hay conexión hacia el servidor", Toast.LENGTH_LONG).show();
            error = true;
        } catch (XmlPullParserException xppe) {
            Log.d("XmlPullParserException",xppe.toString());
        } catch (Exception ex) {
            Log.d("Exception",ex.toString());
            //ex.printStackTrace();
        }
    }
}
